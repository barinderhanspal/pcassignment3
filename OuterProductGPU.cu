//******************************************************************************
//
// File:    OuterProductGPU.cu
// Author:  Alan Kaminsky
// Version: 21-Oct-2013
//
// This source file is copyright (C) 2012 by Parallel Crypto LLC. All rights
// reserved. For further information, contact the author, Alan Kaminsky, at
// alan.kaminsky@parallelcrypto.com.
//
// This source file is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 3 of the License, or (at your option) any
// later version.
//
// This source file is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// A copy of the GNU General Public License is provided in the file gpl.txt. You
// may also obtain a copy of the GNU General Public License on the World Wide
// Web at http://www.gnu.org/licenses/gpl.html.
//
//******************************************************************************

/**
 * Program OuterProductGPU computes the outer product of two vectors in parallel
 * on the GPU. Vectors A and B each contain N elements chosen uniformly at
 * random between 0.0 and 1.0. The outer product is an NxN matrix C, such that
 * C[i][j] = A[i]*B[j]. The program computes the whole matrix C, but for
 * demonstration purposes only prints the four corner elements C[0][0],
 * C[0][N-1], C[N-1][0], C[N-1][N-1].
 *
 * The program stores A, B, and C in global memory on the GPU, and accesses
 * them directly from global memory.
 *
 * Usage: OuterProductGPU <seed> <N>
 * <seed> = Pseudorandom number generator seed
 * <N> = Vector length, N >= 1, must be a multiple of 16
 */

#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>

#include "Util.cu"
#include "Random.cu"

// Thread block dimensions = MxM
#define M 16

//------------------------------------------------------------------------------
// DEVICE FUNCTIONS

/**
 * Device kernel to evaluate the outer product.
 *
 * Called with a two-dimensional grid of two-dimensional blocks, NxN total
 * threads, MxM threads per block.
 *
 * @param  devA  Pointer to vector A in device global memory. An array of N
 *               floats.
 * @param  devB  Pointer to vector B in device global memory. An array of N
 *               floats.
 * @param  devC  Pointer to matrix C in device global memory. A matrix of NxN
 *               floats in row major order.
 * @param  N     Vector and matrix dimension.
 */
__global__ void outerProduct
	(float *devA,
	 float *devB,
	 float *devC,
	 int N)
	{
	int x, y, i, j;

	// Determine thread row y and column x within thread block.
	y = threadIdx.y;
	x = threadIdx.x;

	// Determine matrix element row i and column j.
	i = blockIdx.y*blockDim.y + y;
	j = blockIdx.x*blockDim.x + x;

	// Each thread computes its own matrix element.
	devC[i*N + j] = devA[i]*devB[j];
	}

//------------------------------------------------------------------------------
// HOST FUNCTIONS

/**
 * Print a usage message and exit.
 */
static void usage()
	{
	fprintf (stderr, "Usage: OuterProductGPU <seed> <N>\n");
	fprintf (stderr, "<seed> = Pseudorandom number generator seed\n");
	fprintf (stderr, "<N> = Vector length, N >= 1, must be a multiple of 16\n");
	exit (1);
	}

/**
 * Main program.
 */
int main
	(int argc,
	 char *argv[])
	{
	unsigned long long int seed;
	int N;
	float *A, *B, *C, *devA, *devB, *devC;
	size_t Asize, Bsize, Csize;
	prng_t prng;
	unsigned long long int t1, t2;

	// Parse command line arguments.
	if (argc != 3) usage();
	progname = argv[0];
	if (sscanf (argv[1], "%llu", &seed) != 1) usage();
	if (sscanf (argv[2], "%d", &N) != 1 || N < 1 || N%M != 0) usage();

	// Set CUDA device.
	setCudaDevice();

	// Allocate storage in CPU memory.
	Asize = N*sizeof(float);
	Bsize = N*sizeof(float);
	Csize = N*N*sizeof(float);
	if ((A = (float*) malloc (Asize)) == NULL) die ("Cannot allocate A");
	if ((B = (float*) malloc (Bsize)) == NULL) die ("Cannot allocate B");
	if ((C = (float*) malloc (Csize)) == NULL) die ("Cannot allocate C");

	// Allocate storage in device global memory.
	checkCuda (cudaMalloc (&devA, Asize), "Cannot allocate devA");
	checkCuda (cudaMalloc (&devB, Bsize), "Cannot allocate devB");
	checkCuda (cudaMalloc (&devC, Csize), "Cannot allocate devC");

	// Initialize vectors A and B with random elements.
	prngSetSeed (&prng, seed);
	for (int i = 0; i < N; ++ i)
		{
		A[i] = prngNextFloat (&prng);
		B[i] = prngNextFloat (&prng);
		}

	// Copy vectors A and B from CPU memory to device global memory.
	checkCuda (cudaMemcpy (devA, A, Asize, cudaMemcpyHostToDevice),
		"Cannot copy A to devA");
	checkCuda (cudaMemcpy (devB, B, Bsize, cudaMemcpyHostToDevice),
		"Cannot copy B to devB");

	// Compute outer product in parallel on the device.
	t1 = currentTimeMillis();
	dim3 GRID_DIM (N/M, N/M);
	dim3 BLOCK_DIM (M, M);
	outerProduct <<< GRID_DIM, BLOCK_DIM >>> (devA, devB, devC, N);
	cudaThreadSynchronize();
	checkCuda (cudaGetLastError(), "Cannot launch outerProduct() kernel");
	t2 = currentTimeMillis();

	// Copy matrix C from device global memory to CPU memory.
	checkCuda (cudaMemcpy (C, devC, Csize, cudaMemcpyDeviceToHost),
		"Cannot copy devC to C");

	// Print selected matrix elements.
	printf ("A[0] = %.6f\n", A[0]);
	printf ("A[%d] = %.6f\n", N-1, A[N-1]);
	printf ("B[0] = %.6f\n", B[0]);
	printf ("B[%d] = %.6f\n", N-1, B[N-1]);
	printf ("C[0][0] = %.6f\n", C[0]);
	printf ("C[0][%d] = %.6f\n", N-1, C[N-1]);
	printf ("C[%d][0] = %.6f\n", N-1, C[(N-1)*N]);
	printf ("C[%d][%d] = %.6f\n", N-1, N-1, C[(N-1)*N+(N-1)]);
	printf ("%llu msec computation\n", t2 - t1);
	}
