#!/bin/bash

OLD_PATH=${PATH}
OLD_LD_LIBRARY_PATH=${LD_LIBRARY_PATH}

PATH=/usr/local/versions/jdk1.7.0_11/bin:/usr/local/dcs/versions/jdk1.7.0_11_x64/bin:/usr/local/dcs/versions/jdk1.7.0_11/bin:/usr/local/cuda/bin:${PATH}

if [ PATH != OLD_PATH ]
	then
	echo "PATH has been set successfully"
else
	echo "Failed to set PATH !!"
fi

LD_LIBRARY_PATH=/usr/local/cuda/lib:/usr/local/cuda/lib64:${LD_LIBRARY_PATH}

if [ LD_LIBRARY_PATH != OLD_LD_LIBRARY_PATH ]
	then
	echo "LD_LIBRARY_PATH was set successfully"
else
	echo "Failed to set LD_LIBRARY_PATH !!"
fi
