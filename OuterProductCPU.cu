//******************************************************************************
//
// File:    OuterProductCPU.cu
// Author:  Alan Kaminsky
// Version: 19-Jan-2012
//
// This source file is copyright (C) 2012 by Parallel Crypto LLC. All rights
// reserved. For further information, contact the author, Alan Kaminsky, at
// alan.kaminsky@parallelcrypto.com.
//
// This source file is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 3 of the License, or (at your option) any
// later version.
//
// This source file is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// A copy of the GNU General Public License is provided in the file gpl.txt. You
// may also obtain a copy of the GNU General Public License on the World Wide
// Web at http://www.gnu.org/licenses/gpl.html.
//
//******************************************************************************

/**
 * Program OuterProductCPU computes the outer product of two vectors
 * sequentially on the CPU. Vectors A and B each contain N elements chosen
 * uniformly at random between 0.0 and 1.0. The outer product is an NxN matrix
 * C, such that C[i][j] = A[i]*B[j]. The program computes the whole matrix C,
 * but for demonstration purposes only prints the four corner elements C[0][0],
 * C[0][N-1], C[N-1][0], C[N-1][N-1].
 *
 * Usage: OuterProductCPU <seed> <N>
 * <seed> = Pseudorandom number generator seed
 * <N> = Vector length, N >= 1, must be a multiple of 16
 */

#include <stdlib.h>
#include <stdio.h>

#include "Util.cu"
#include "Random.cu"

// Thread block dimensions = MxM
#define M 16

/**
 * Print a usage message and exit.
 */
static void usage()
	{
	fprintf (stderr, "Usage: OuterProductCPU <seed> <N>\n");
	fprintf (stderr, "<seed> = Pseudorandom number generator seed\n");
	fprintf (stderr, "<N> = Vector length, N >= 1, must be a multiple of 16\n");
	exit (1);
	}

/**
 * Main program.
 */
int main
	(int argc,
	 char *argv[])
	{
	unsigned long long int seed;
	int N;
	float *A, *B, *C;
	size_t Asize, Bsize, Csize;
	prng_t prng;
	unsigned long long int t1, t2;

	// Parse command line arguments.
	if (argc != 3) usage();
	progname = argv[0];
	if (sscanf (argv[1], "%llu", &seed) != 1) usage();
	if (sscanf (argv[2], "%d", &N) != 1 || N < 1 || N%M != 0) usage();

	// Allocate storage in CPU memory.
	Asize = N*sizeof(float);
	Bsize = N*sizeof(float);
	Csize = N*N*sizeof(float);
	if ((A = (float*) malloc (Asize)) == NULL) die ("Cannot allocate A");
	if ((B = (float*) malloc (Bsize)) == NULL) die ("Cannot allocate B");
	if ((C = (float*) malloc (Csize)) == NULL) die ("Cannot allocate C");

	// Initialize vectors A and B with random elements.
	prngSetSeed (&prng, seed);
	for (int i = 0; i < N; ++ i)
		{
		A[i] = prngNextFloat (&prng);
		B[i] = prngNextFloat (&prng);
		}

	// Compute outer product sequentially on the CPU. Measure computation time.
	t1 = currentTimeMillis();
	for (int i = 0; i < N; ++ i)
		{
		float A_i = A[i];
		float *C_i = &C[i*N];
		for (int j = 0; j < N; ++ j)
			{
			C_i[j] = A_i*B[j];
			}
		}
	t2 = currentTimeMillis();

	// Print selected matrix elements.
	printf ("A[0] = %.6f\n", A[0]);
	printf ("A[%d] = %.6f\n", N-1, A[N-1]);
	printf ("B[0] = %.6f\n", B[0]);
	printf ("B[%d] = %.6f\n", N-1, B[N-1]);
	printf ("C[0][0] = %.6f\n", C[0]);
	printf ("C[0][%d] = %.6f\n", N-1, C[N-1]);
	printf ("C[%d][0] = %.6f\n", N-1, C[(N-1)*N]);
	printf ("C[%d][%d] = %.6f\n", N-1, N-1, C[(N-1)*N+(N-1)]);
	printf ("%llu msec computation\n", t2 - t1);
	}
