//******************************************************************************
//
// File:    PiCPU.cu
// Author:  Alan Kaminsky
// Version: 22-Oct-2013
//
// This source file is copyright (C) 2013 by Parallel Crypto LLC. All rights
// reserved. For further information, contact the author, Alan Kaminsky, at
// alan.kaminsky@parallelcrypto.com.
//
// This source file is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 3 of the License, or (at your option) any
// later version.
//
// This source file is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// A copy of the GNU General Public License is provided in the file gpl.txt. You
// may also obtain a copy of the GNU General Public License on the World Wide
// Web at http://www.gnu.org/licenses/gpl.html.
//
//******************************************************************************

/**
 * Program PiCPU computes an approximation of pi sequentially on the CPU by
 * generating N random (x,y) points in the unit square and counting how many
 * fall within a distance of 1 from the origin.
 *
 * Usage: PiCPU <seed> <N>
 * <seed> = Pseudorandom number generator seed
 * <N> = Number of points, N >= 1
 */

#include <stdlib.h>
#include <stdio.h>

#include "Util.cu"
#include "Random.cu"

/**
 * Print a usage message and exit.
 */
static void usage()
	{
	fprintf (stderr, "Usage: PiCPU <seed> <N>\n");
	fprintf (stderr, "<seed> = Pseudorandom number generator seed\n");
	fprintf (stderr, "<N> = Number of points, N >= 1\n");
	exit (1);
	}

/**
 * Main program.
 */
int main
	(int argc,
	 char *argv[])
	{
	unsigned long long int seed, N, count, t1, t2;
	prng_t prng;

	// Parse command line arguments.
	if (argc != 3) usage();
	progname = argv[0];
	if (sscanf (argv[1], "%llu", &seed) != 1) usage();
	if (sscanf (argv[2], "%llu", &N) != 1 || N < 1) usage();

	// Initialize prng and count.
	prngSetSeed (&prng, seed);
	count = 0;

	// Compute random points sequentially on the CPU. Measure computation time.
	t1 = currentTimeMillis();
	for (unsigned long long int i = 0; i < N; ++ i)
		{
		double x = prngNextDouble (&prng);
		double y = prngNextDouble (&prng);
		if (x*x + y*y <= 1.0) ++ count;
		}
	t2 = currentTimeMillis();

	// Print results.
	printf ("pi = 4*%llu/%llu = %.9f\n", count, N, 4.0*count/N);
	printf ("%llu msec\n", t2 - t1);
	}
