all : OuterProductCPU \
	OuterProductGPU \
	PiCPU \
	PiGPU \
	ZombieCPU \
	ZombieGPU \
	DiversityIndex

OuterProductCPU : OuterProductCPU.cu Util.cu Random.cu
	nvcc -o OuterProductCPU OuterProductCPU.cu

OuterProductGPU : OuterProductGPU.cu Util.cu Random.cu
	nvcc -arch compute_20 -code compute_20,sm_20 -o OuterProductGPU OuterProductGPU.cu

PiCPU : PiCPU.cu Util.cu Random.cu
	nvcc -o PiCPU PiCPU.cu

PiGPU : PiGPU.cu Util.cu Random.cu
	nvcc -arch compute_20 -code compute_20,sm_20 -o PiGPU PiGPU.cu

ZombieCPU : ZombieCPU.cu Util.cu Random.cu
	nvcc -o ZombieCPU ZombieCPU.cu

ZombieGPU : ZombieGPU.cu Util.cu Random.cu
	nvcc -arch compute_20 -code compute_20,sm_20 -o ZombieGPU ZombieGPU.cu

DiversityIndex : DiversityIndex.cu Util.cu Random.cu
	nvcc -arch compute_20 -code compute_20,sm_20 -o DiversityIndex DiversityIndex.cu

clean :
	rm -f OuterProductCPU
	rm -f OuterProductGPU
	rm -f PiCPU
	rm -f PiGPU
	rm -f ZombieCPU
	rm -f ZombieGPU
	rm -f DiversityIndex
