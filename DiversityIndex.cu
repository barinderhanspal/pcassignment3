//******************************************************************************
//
// File:    DiversityIndex.cu
// Author:  Barinderpal Singh Hanspal (bxh5868@rit.edu)
//
//******************************************************************************

/**
 * Program DiversityIndex computes the diversity index for a given population set.
 * Diversity index is the probability that the two different individuals chosen from
 * the given population set fall into different categories. The categories can be
 * anything - race, religion, nationality, etc. 
 *
 * This program calculates the Diversity Index for the given input population by
 * running a Monte Carlo simulation. The program is given the number of individuals
 * in each category. The program does a large number of trials. In each trial, 
 * the program picks two different individuals at random from the total population;
 * determines each individual's category; and increments a counter if the categories
 * are different. At the end, the program calculates the diversity index 
 * (in percent) as 100×counter÷trials. 
 *
 * Usage: ./DiversityIndex <seed> <trials> <pop1> <pop2> ...
 * where,
 * 	
 	<seed> 	          ->   is the pseudorandom number generator seed.
 *	<trials> 	  ->   is the number of trials. It must be a 64-bit integer ≥ 1.
 *   	<pop1>, <pop2>,.. ->   give the number of individuals in each category; 
 *			       each number must be a 32-bit integer ≥ 1; 
 *			       there must be one or more categories. 
 */

#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>

#include "Util.cu"
#include "Random.cu"

//------------------------------------------------------------------------------
// DEVICE FUNCTIONS

// Number of threads per block.
#define NT 1024

// Overall counter variable in global memory.
__device__ unsigned long long int devCount;

// Per-thread counter variables in shared memory.
__shared__ unsigned long long int shrCount [NT];

/**
 * Device kernel that calculateS the diversity index.
 *
 * @param  seed		  Pseudorandom number generator seed.
 * @param  trials	  Number of trials.
 * @param  population     Array that stores population info.
 * @param  numCategories  No. of categories/ Size of population array.
 *
 */
__global__ void computeDiversityIndex
    (unsigned long long int seed,
     unsigned long long int trials,
     unsigned int *population,
     unsigned int numCategories) {
     
    int x, size, rank;
    unsigned long long int len, lb, ub, count;
    unsigned int totalPopSize; // Total population size

    prng_t prng;
    
    totalPopSize = population[numCategories-1]; // set the total population size
    
    // Determine number of threads and this thread's rank.
    x = threadIdx.x;
    size = gridDim.x*NT;
    rank = blockIdx.x*NT + x;

    // Determine iterations for this thread.
    len = (trials + size - 1)/size;
    lb = rank*len;
    ub = min (lb + len, trials) - 1;

    // Initialize per-thread prng and count.
    prngSetSeed (&prng, seed + rank);
    count = 0;
    
    // Compute diversity index by using the prngNextIndex() function.
    for (unsigned long long int i = lb; i <= ub; ++ i)
        {
        unsigned int cand1Index = 0; // Category that Candidate 1 belongs to
        unsigned int cand2Index = 0; // Category that Candidate 2 belongs to
        
        unsigned int candidate1 = 0; // Value used to calculate Candidate1's category
        unsigned int candidate2 = 0; // Value used to calculate Candidate2's category
        
        /*
         * Generate random ints for candidate1 and candidate2 such that they are not
         * the same. prngNextInt() generates random numbers in range [0, n-1]; where
         * n is the parameter provided to this function.
         */
        while( (candidate1 = (int) prngNextInt(&prng, (int) totalPopSize))
		== (candidate2 = (int) prngNextInt(&prng, (int) totalPopSize)) );
        
        /*
         * We increment the candidate variables to adjust for corner cases.
         * For case when prngNextInt returns 0;
         * For case when prngNextInt can only return a max value of n-1.
         */   		
        candidate1++; 
        candidate2++;
     
     	// Find the category for candidate 1
        for(unsigned int index = 0; index < numCategories; index ++){
            if(population[index] >= candidate1){
            	// If candidate lies in a particular population. Get its category
                cand1Index = index;
                break;            
            }
        }        

	// FInd the category for candidate 2
        for(unsigned int index = 0; index < numCategories; index ++){
            if(population[index] >= candidate2){
                cand2Index = index;
		// If candidate lies in a particular population. Get its category
                break;            
            }
        }        

        if(cand1Index != cand2Index) {
        	// If candidates belong to different categories. Increment counter
        	++ count;
        }
     }

    // Shared memory parallel reduction within thread block.
    shrCount[x] = count;
    __syncthreads();
    for (int i = NT/2; i > 0; i >>= 1)
        {
        if (x < i)
            shrCount[x] += shrCount[x+i];
        __syncthreads();
        }

    // Atomic reduction into overall counter.
    if (x == 0)
        atomicAdd (&devCount, shrCount[0]);
}

//------------------------------------------------------------------------------
// HOST FUNCTIONS

/**
 * Print a usage message and exit.
 */
static void usage() {

    fprintf (stderr, "Usage: DiversityIndex <seed> <trials> <pop1> <pop2> ...\n");
    fprintf (stderr, "<seed> = Pseudorandom number generator seed\n");
    fprintf (stderr, "<trials> = Number of trials, trials >= 1\n");
    fprintf (stderr, "<pop1>, <pop2> = Number of individuals in each catergory,\n"
                , "Each population value should be >= 1\n"
                , "There must be one or more categories\n");
    exit (1);
}

/**
 * Main program.
 */
int main (int argc, char *argv[]) {
    
    unsigned long long int seed, trials, hostCount;
    int dev, NB;
    
    /* 
     * devPop        -> Array that stores the population array on graphic device
     * population    -> Array that stores the population array on the host
     * numCategories -> integer that stores the number of population categories
     */
    unsigned int *devPop, *population, numCategories;
    
    size_t sizeofPopulationArray; // Stores the size of population array for memory alloc.
    
    // Check if number of args match expected args
    if (argc < 4) 
        usage();
    
    // Determine the number of categories inputted from the command line
    numCategories = (unsigned int)argc - 3;
    trials = 0;
    hostCount = 0;
    
    // Check if there is only one category with only one individual
    if(numCategories == 1 && atoi(argv[argc - 1]) == 1){
        printf ("Diversity index = %llu/%llu = %.1f\n",
        	hostCount, trials, 100.0*hostCount/(trials + 1));
        return 0;
    }
    
    // Calculate the size of population array for which memory needs to be allocated 
    sizeofPopulationArray = numCategories * sizeof(unsigned int);

    if((population = (unsigned int*) malloc (sizeofPopulationArray)) == NULL)
        die ("Cannot allocate population array");
        
    // Set seed        
    if (sscanf (argv[1], "%llu", &seed) != 1) 
        usage();
    
    // Check if value for trials is less than 1, if not then set trails
    if (sscanf (argv[2], "%llu", &trials) != 1 || trials < 1)
         usage();

    // Check if population for 1st category is less than 1, if not set population[0]
    if (sscanf (argv[3], "%u", &population[0]) != 1 || population[0] < 1)
        usage();     

    /* 
     * Read population for all categories. Each location in array stores the 
     * value of population read from the command line along with the sum of the
     * population value in its previous location in the array
     */
    for(int popIndex = 1; popIndex < numCategories; popIndex ++){
        
        // Check if value for population is less than 1
        if (sscanf (argv[popIndex + 3], "%u", &population[popIndex]) != 1 
            || population[popIndex] < 1)
            usage();     
            
        population[popIndex] += population[popIndex - 1]; // Add previous value
    }

    // Set CUDA device and determine number of multiprocessors (thread blocks).
    dev = setCudaDevice();

    // Allocate memory for population array on graphic device.    
    checkCuda ( cudaMalloc (&devPop, sizeofPopulationArray), "Cannot alloc devPop" );
    
     // Copy array 'population' from CPU memory to device global memory.
    checkCuda ( cudaMemcpy (devPop, population, sizeofPopulationArray, cudaMemcpyHostToDevice),
    		 "Cannot copy population array to device" );
    		 
    checkCuda ( cudaDeviceGetAttribute (&NB, cudaDevAttrMultiProcessorCount, dev),
        	 "Cannot get number of multiprocessors" );

    // Initialize overall counter.
    hostCount = 0;
    checkCuda (cudaMemcpyToSymbol (devCount, &hostCount, sizeof(hostCount)),
         	"Cannot initialize devCount");

    // Compute the Diversity Index for the population
    computeDiversityIndex <<< NB, NT >>> (seed, trials, devPop, numCategories);
    cudaThreadSynchronize();
    
    checkCuda (cudaGetLastError(), "Cannot launch computeDiversityIndex() kernel");

    // Get the value of Diversity Index from GPU.
    checkCuda (cudaMemcpyFromSymbol (&hostCount, devCount, sizeof(hostCount)),
         	"Cannot copy devCount to hostCount");

    // Print results.
    printf ("Diversity index = %llu/%llu = %.1f\n", hostCount, trials, 100.0*hostCount/trials);
    
    return 0;
}
